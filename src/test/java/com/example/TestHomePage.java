package com.example;

import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Test;

/**
 * Simple test using the WicketTester
 */
public class TestHomePage {
	private WicketTester tester;

	@Before
	public void setUp() {
		tester = new WicketTester(new WicketApplication());
	}

	@Test
	public void homepageRendersSuccessfully() {
		// start and render the test page
		tester.startPage(WicketJaUser690.class);
		// assert rendered page class
		tester.assertRenderedPage(WicketJaUser690.class);
	}

	@Test
	public void storeを選択しamountを選択する() {
		tester.startPage(WicketJaUser690.class);
		FormTester formTester = tester.newFormTester("inputForm", true);
		formTester.select("store", 0);
		tester.executeAjaxEvent("inputForm:store", "onchange");
		tester.assertModelValue("inputForm:store", "店舗A");
		formTester.select("amount", 0);
		tester.executeAjaxEvent("inputForm:amount", "onchange");
		tester.assertFeedback("inputForm:amountFeedBack", "AmountOverValidator");
		tester.assertModelValue("inputForm:amount", null);
	}

	@Test
	public void amountバリデーションが動作するか() {
		tester.startPage(WicketJaUser690.class);
		FormTester formTester = tester.newFormTester("inputForm", true);
		formTester.select("amount", 0);
		tester.executeAjaxEvent("inputForm:amount", "onchange");
		tester.assertModelValue("inputForm:amount", "100");
		formTester.select("store", 0);
		tester.executeAjaxEvent("inputForm:store", "onchange");
		tester.assertFeedback("inputForm:amountFeedBack", "AmountOverValidator");
		tester.assertModelValue("inputForm:store", null);
	}
}
