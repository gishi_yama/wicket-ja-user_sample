package com.example;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.validator.AbstractValidator;

public class WicketJaUser690 extends WebPage {

	private final Form<Void> myForm = new Form<>("inputForm");

	private final DropDownChoice<String> store = new DropDownChoice<String>("store",
			new Model<String>(), Arrays.asList("店舗A", "店舗B"));

	private final DropDownChoice<String> amount = new DropDownChoice<String>("amount",
			new Model<String>(), Arrays.asList("100", "200", "300", "400", "500"));

	FeedbackPanel amountFeedBack;

	public WicketJaUser690() {
		this.add(myForm);
		myForm.add(store);

		// 店舗別金額Overバリデータ
		AmountOverValidator aov = new AmountOverValidator(amount, store);
		amount.add(aov);
		store.add(aov);

		// 金額フィードバックパネル
		amountFeedBack = new FeedbackPanel("amountFeedBack");
		amountFeedBack.setOutputMarkupId(true);

		myForm.add(amount);
		myForm.add(amountFeedBack);

		store.add(new AjaxFormComponentUpdatingBehavior("onchange") {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				target.add(amountFeedBack);
			}

			@Override
			protected void onError(AjaxRequestTarget target, RuntimeException e) {
				super.onError(target, e);
				target.add(amountFeedBack);
			}
		});

		amount.add(new AjaxFormComponentUpdatingBehavior("onchange") {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				target.add(amountFeedBack);
			}

			@Override
			protected void onError(AjaxRequestTarget target, RuntimeException e) {
				super.onError(target, e);
				target.add(amountFeedBack);
			}
		});

	}

	public class AmountOverValidator extends AbstractValidator<String> {
		private DropDownChoice<String> store;
		private DropDownChoice<String> amount;

		public AmountOverValidator(DropDownChoice<String> amount, DropDownChoice<String> store) {
			this.store = store;
			this.amount = amount;
		}

		@Override
		protected void onValidate(IValidatable<String> validatable) {
			// 入力値を取得
			String strStore = this.store.getConvertedInput();
			String strAmount = this.amount.getConvertedInput();

			if (strStore == null) {
				strStore = store.getModelObject();
			}
			if (strAmount == null) {
				strAmount = amount.getModelObject();
			}

			if ((StringUtils.isEmpty(strStore)) || (StringUtils.isEmpty(strAmount))) {
			} else {
				// 店舗別の金額確認処理
				// エラー時のとき
				error(validatable, "AmountOverValidator");
			}
		}
	}
}